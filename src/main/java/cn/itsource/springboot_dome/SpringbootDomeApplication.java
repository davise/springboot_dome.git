package cn.itsource.springboot_dome;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**=================================================================
 * 步骤：
 *      1、@SpringBootApplication：申明主配置类
 *      2、@MapperScan("cn.itsource.springboot_dome.mapper")：扫描mapper接口的路径
 ==================================================================*/
@SpringBootApplication
@MapperScan("cn.itsource.springboot_dome.mapper")
public class SpringbootDomeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDomeApplication.class, args);
    }

}
