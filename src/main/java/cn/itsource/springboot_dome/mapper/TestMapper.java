package cn.itsource.springboot_dome.mapper;

import cn.itsource.springboot_dome.domain.Employee;

import java.util.List;

public interface TestMapper {
    List<Employee> select();
}
