package cn.itsource.springboot_dome.web.controller;

import cn.itsource.springboot_dome.domain.Employee;
import cn.itsource.springboot_dome.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/test_springboot")
public class TestController {

    @Autowired
    private ITestService service;

    @GetMapping("/test")
    public List<Employee> select(){
        return service.select();
    }
}
