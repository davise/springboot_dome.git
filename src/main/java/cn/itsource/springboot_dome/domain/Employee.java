package cn.itsource.springboot_dome.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    private Long id;
    private String username;
    private String email;
    private String phone;
    private String password;
    private Integer age;
    private Integer state;
    private String loginInfoId;
    private String  salt;
    private Date createTime;
    private String  headImg;

}
