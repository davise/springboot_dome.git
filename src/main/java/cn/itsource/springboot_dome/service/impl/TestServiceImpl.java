package cn.itsource.springboot_dome.service.impl;

import cn.itsource.springboot_dome.domain.Employee;
import cn.itsource.springboot_dome.mapper.TestMapper;
import cn.itsource.springboot_dome.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements ITestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public List<Employee> select() {

        return testMapper.select();
    }
}
