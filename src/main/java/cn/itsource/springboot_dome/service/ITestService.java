package cn.itsource.springboot_dome.service;

import cn.itsource.springboot_dome.domain.Employee;

import java.util.List;

public interface ITestService {


    List<Employee> select();

}
