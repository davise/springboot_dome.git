# 基于maven的SpringBoot、SpringMVC、MyBatis的项目搭建



## 一、创建一个maven项目：

![image-20210705191731849](./readme/image-20210705191731849.png)

### 		next

![image-20210705192028029](./readme/image-20210705192028029.png)

### 		最后：项目基础目录（这是所有准备工作完成后的目录）

![image-20210703155708733](./readme/image-20210703155708733.png)

------



## 二、项目一些简单的配置：

### 1、pom.xml：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" 
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    
    <modelVersion>4.0.0</modelVersion>
    
     <!--springboot的父工程：管理相当多jar的不冲突的版本-->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.5.RELEASE</version>
    </parent>

    <groupId>cn.itsource</groupId>
    <artifactId>springboot_dome</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>springboot_dome</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
        </dependency>

        <!-- spring-boot整合mybatis -->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.1.1</version>
        </dependency>

        <!--连接池：阿里的德鲁伊连接池-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.1.6</version>
        </dependency>

        <!-- mysql驱动 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.38</version>
        </dependency>

        
        
        <!--以下是非必须（只是一些工具）-->
        <!--Lomkob--> <!--在实体类上加上@Data，我们就不用写get和set-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.16.18</version>
        </dependency>

        <!--阿里巴巴的json转换工具jar：fastjson-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.4</version>
        </dependency>

        <!--批量读取配置文件的信息；用于@ConfigurationProperties注解-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>

    </dependencies>

    <!--  打包方式  -->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>

```

### 2、主启动类：

@MapperScan("cn.itsource.springboot_dome.mapper")：扫描mapper接口的路径

```java
/**=================================================================
 * 步骤：
 *      1、@SpringBootApplication：申明主配置类
 *      2、@MapperScan("cn.itsource.springboot_dome.mapper")：扫描mapper接口的路径
 ==================================================================*/
@SpringBootApplication
@MapperScan("cn.itsource.springboot_dome.mapper")
public class SpringbootDomeApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootDomeApplication.class, args);
    }
}
```

### 3、application.yml：

```yml
#后台端口
server:
  port: 80

#连接
spring:
  #项目数据库的连接
  #注意：当只有一个连接（datasource）时，spring会默认连接，并且是使用 url
  #     当时当有两个连接时，需要一个连接配置类：QuartzDataSourceConfig，并且使用 jdbcUrl（这里你不需要注意）
  #
  # 这里注意要根据你的数据库信息更改数据
  datasource:
    username: root
    password: adminpass
    url: jdbc:mysql://localhost:3306/pethome?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false
    driver-class-name: com.mysql.jdbc.Driver
    #使用这个DruidDataSource，必须在pom中使用 ————————》连接池：阿里的德鲁伊连接池 druid
    type: com.alibaba.druid.pool.DruidDataSource

#mybatis的mapper.xml路径
mybatis:
  mapper-locations: classpath:cn/itsource/pethome/*/mapper/*Mapper.xml
  #别名
  #type-aliases-package:
```

### 4、云共享：

​	码云仓库：https://gitee.com/davise/springboot_dome

​	码云下载地址：https://gitee.com/davise/springboot_dome.git



## 三、集成MySQL8

只需修改下面两个地方

1、导入依赖（更换版本）

```xml
<!-- mysql驱动 -->
<!-- <dependency>-->
<!--   <groupId>mysql</groupId>-->
<!--     <artifactId>mysql-connector-java</artifactId>-->
<!--   <version>5.1.38</version>-->
<!-- </dependency>

<dependency>
  <groupId>mysql</groupId>
  <artifactId>mysql-connector-java</artifactId>
  <version>8.0.11</version>
</dependency>
```

2、url

```yml
   
# driver-class-name: com.mysql.jdbc.Driver
driver-class-name: com.mysql.cj.jdbc.Driver
```

